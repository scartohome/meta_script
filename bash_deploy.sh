#!/usr/bin/env bash
echo '--------------------------------------------'
echo "Invalidating cloudfrond distribution to get fresh cache"
aws cloudfront create-invalidation --distribution-id=E1E35JL6E3L57G --paths / --profile=default
echo '--------------------------------------------'
echo "Deployment complete"

