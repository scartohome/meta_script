#! /bin/bash
#export AWS_ACCESS_KEY_ID=“AAKIAI7VMPIIM4GZVUIFQ"
#export AWS_SECRET_ACCESS_KEY="Cr9ynI8u3Tuum7ay+3f8NbYJZaAxJyxpRaJS/IyO"

echo '********************************************'
echo '********************************************'
echo '***          MAIN SCRIPT START           ***'
echo '********************************************'
echo '********************************************'

CWD="$(pwd)"
CDN="${CDN:-dev.ikodesh.org-E1E35JL6E3L57G}"
echo "Working remote CDN is : $CDN"
echo "Working remote DIR is : $CWD"
echo '--------------------------------------------'
Path_var=$(echo $CDN | cut -f1 -d-)
echo "Working remote PATH is : $Path_var"
echo '--------------------------------------------'
Distribution_var=$(echo $CDN | cut -f2 -d-)
echo "Working remote PATH is : $Distribution_var"
echo '--------------------------------------------'

echo '--------------------------------------------'
echo "Invalidating cloudfrond distribution to clear cache"
DATE=`date '+%Y-%m-%d %H:%M:%S'`
echo "$DATE START cloudfront create-invalidation" >> "log/log.alg"
aws cloudfront create-invalidation --distribution-id=$Distribution_var --paths / --profile=default
echo '--------------------------------------------'

echo 'REMOVING old *.meta files -> rm -rf meta/*'
rm -rf meta/*
echo 'REMOVING old *.json remote files -> rm -rf json_remote/*'
rm -rf json_remote/*
echo 'REMOVING old *.json s3 files -> rm -rf json_s3/*'
rm -rf json_s3/*
echo 'REMOVING old *.json files -> rm -rf json/*'
rm -rf json/*
echo 'REMOVING old *.json DIFF -> rm -rf json_diff/*'
rm -rf json_diff/*
echo 'REMOVING old *TIMESTAMP.json -> rm -rf timestamp/*'
rm -rf timestamp/*

echo '--------------------------------------------'
echo 'Creating folders'
mkdir -p $CWD/meta;
mkdir -p $CWD/json;
mkdir -p $CWD/json_remote;
mkdir -p $CWD/json_s3;
mkdir -p $CWD/json_diff;
mkdir -p $CWD/timestamp;
echo '--------------------------------------------'
echo 'Starting to copy *.meta from s3... --quiet'
#aws s3 sync s3://$Path_var/drive/courses/ meta/ --exclude "*" --include "*.meta"
aws s3 cp s3://$Path_var/drive/courses/ meta/ --recursive --exclude "*" --include "*.meta" --quiet
echo 'End copy *.meta from s3...'
echo '--------------------------------------------'
echo 'Copy remote *.json files'
echo '--------------------------------------------'
aws s3 cp s3://$Path_var/drive/courses/ json_remote/ --recursive --exclude "*" --include "*.json" --quiet

echo '--------------------------------------------'
echo 'SYNC FROM S3 *.meta files - DONE'
echo 'Running Node script'
echo '--------------------------------------------'
node script.js
echo '--------------------------------------------'
echo 'Node script ended...'
echo '--------------------------------------------'
echo 'Change log - TRUE if meta json files UNCHANGED from last update'
cat $CWD/json_diff/diff.json
line2="`sed -n 2p json_diff/diff_comb.json`"
echo "COMBINED DIFF is ::: $line2"
echo '--------------------------------------------'
