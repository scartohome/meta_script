var path = require('path');
var fs = require('fs');
var glob = require("glob");
var jsonfile = require('jsonfile');
var exec = require('child_process').exec;


exports.handler_sh = function(event, context, callback) {
  var child = exec('./bash_script.sh', function(error, stdout, stderr){
    if (error) {
      callback(error);
    }
    callback(null,stdout);
  });
};