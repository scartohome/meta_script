#! /bin/bash
#export AWS_ACCESS_KEY_ID=“AAKIAI7VMPIIM4GZVUIFQ"
#export AWS_SECRET_ACCESS_KEY="Cr9ynI8u3Tuum7ay+3f8NbYJZaAxJyxpRaJS/IyO"

echo '********************************************'
echo '********************************************'
echo '***          MAIN SCRIPT START           ***'
echo '********************************************'
echo '********************************************'

CWD="$(pwd)"
#CDN="${CDN:-staging.compedia.net-E1E35JL6E3L57G}"
#CDN="${CDN:-temp.compedia.net-E15D0TKN9S9WDM}"
CDN="${CDN:-automation-testsbuket-E15D0TKN9S9WDM}"
#CDN="${CDN:-dev.ikodesh.org-E1E35JL6E3L57G}"
echo "Working remote CDN is : $CDN"
echo "Working remote DIR is : $CWD"
echo '--------------------------------------------'
Path_var="automation-testsbuket"
echo "Working remote PATH is : $Path_var"
echo '--------------------------------------------'
Distribution_var="E15D0TKN9S9WDM"
echo "Working remote PATH is : $Distribution_var"
echo '--------------------------------------------'

echo '--------------------------------------------'
echo "ON START Invalidating cloudfrond distribution to clear cache"
DATE=`date '+%Y-%m-%d %H:%M:%S'`
echo "$DATE START cloudfront create-invalidation $CDN" >> "log/log.alg"
aws cloudfront create-invalidation --distribution-id=$Distribution_var --paths / --profile=default
echo '--------------------------------------------'

echo 'REMOVING old *.json remote files -> rm -rf json_remote/*'
rm -rf json_remote/*
echo 'REMOVING old *.json s3 files -> rm -rf json_s3/*'
rm -rf json_s3/*
echo 'REMOVING old *.json UPDATED -> rm -rf updated/*'
rm -rf updated/*
echo 'REMOVING old *.json DIFF -> rm -rf json_diff/*'
rm -rf json_diff/*
echo 'REMOVING old *.meta files -> rm -rf meta/*'
rm -rf meta/*
echo 'REMOVING old *.json files -> rm -rf json/*'
rm -rf json/*
echo 'REMOVING old *.alg LIST -> rm -rf list/*'
rm -rf list/*

echo '--------------------------------------------'
echo 'Creating folders'
mkdir -p $CWD/json_remote;
mkdir -p $CWD/timestamp;
mkdir -p $CWD/json_diff;
mkdir -p $CWD/json_s3;
mkdir -p $CWD/updated;
mkdir -p $CWD/meta;
mkdir -p $CWD/json;
mkdir -p $CWD/list;
mkdir -p $CWD/log;

touch $CWD/list/list.alg
echo '--------------------------------------------'
echo 'Copy *.meta from S3... --quiet'
aws s3 cp s3://$Path_var/drive/courses/ meta/ --recursive --exclude "*" --include "*.meta" --quiet &
echo '--------------------------------------------'
echo 'Copy remote *.json files from S3 --quiet'
echo '--------------------------------------------'
aws s3 cp s3://$Path_var/drive/courses/ json_remote/ --recursive --exclude "*" --include "*.json" --quiet &

wait

echo '--------------------------------------------'
echo 'SYNC FROM S3 *.meta files - DONE'
echo 'Running NodeJS script'
echo '--------------------------------------------'
node script.js
echo '--------------------------------------------'
echo 'Node script ended...'
echo '--------------------------------------------'
echo 'Change LOG - TRUE if meta files CHANGED from last update'
cat $CWD/json_diff/diff_comb.json
line2="`sed -n 2p json_diff/diff_comb.json`"
echo "COMBINED DIFF is ::: $line2"
echo '--------------------------------------------'
echo "COURSES from list :"
while IFS='' read -r line || [[ -n "$line" ]]; do
    echo "$line"
done < "list/list.alg"

echo '--------------------------------------------'

if [[ $line2 = *"false"* ]]; then
  echo -e "NO CHANGES FOUND IN *.meta -> NO CHANGES in generated JSON files
  \n -possible to DISABLE aws cloudfront create-invalidation)
  \n -possible NOT to upload unchanged jsons to S3 \n"
  echo -e "BASH list of COURSES and FILES"
  while IFS='' read -r course || [[ -n "$course" ]]; do
    echo '--------------------------------------------'
    echo "$course"
    COURSE=${course}
    EXT1='.json'
    EXT2='_timestamp.json'
    JSON=${COURSE}${EXT1}
    TIMESTAMP=${COURSE}${EXT2}
    echo $JSON
    echo $TIMESTAMP
  done < "list/list.alg"
else
  echo -e "CHANGES FOUND IN *.meta -> UPLOADING json's + timestamp's S3"
  echo '--------------------------------------------'
  echo -e "BASH list of COURSES"
  echo '--------------------------------------------'
  while IFS='' read -r course || [[ -n "$course" ]]; do
    echo "$course"
    COURSE=${course}
    EXT1='.json'
    EXT2='_timestamp.json'
    JSON=${COURSE}${EXT1}
    TIMESTAMP=${COURSE}${EXT2}
    aws s3 cp json/$JSON s3://$Path_var/drive/courses/$COURSE/ &
    aws s3 cp timestamp/$TIMESTAMP s3://$Path_var/drive/courses/$COURSE/ &
    wait
  done < "list/list.alg"
  echo '--------------------------------------------'
fi

echo '--------------------------------------------'
echo "ON END Invalidating cloudfrond distribution to get fresh cache"
DATE=`date '+%Y-%m-%d %H:%M:%S'`
echo "$DATE END cloudfront create-invalidation $CDN" >> "log/log.alg"
aws cloudfront create-invalidation --distribution-id=$Distribution_var --paths / --profile=default
echo '--------------------------------------------'
echo "Deployment complete"

echo '--------------------------------------------'
echo 'S3 Bucket : check json files date/timestamp :'
echo '--------------------------------------------'

while IFS='' read -r course || [[ -n "$course" ]]; do
  echo '--------------------------------------------'
  echo 'Local copy from S3 after update:'
  echo '--------------------------------------------'
  echo "$course"
  COURSE=${course}
  EXT1='.json'
  EXT2='_timestamp.json'
  JSON=${COURSE}${EXT1}
  TIMESTAMP=${COURSE}${EXT2}
  aws s3 ls s3://$Path_var/drive/courses/$COURSE/$JSON
  if [ $? -ne 0 ]; then
    echo "CHECK: JSON $JSON NOT FOUND on S3"
  fi
  aws s3 ls s3://$Path_var/drive/courses/$COURSE/$TIMESTAMP
  if [ $? -ne 0 ]; then
    echo "CHECK: JSON $TIMESTAMP NOT FOUND on S3"
  fi
  aws s3 cp s3://$Path_var/drive/courses/$COURSE/$JSON json_s3/ &
  aws s3 cp s3://$Path_var/drive/courses/$COURSE/$TIMESTAMP json_s3/ &
  wait
done < "list/list.alg"
