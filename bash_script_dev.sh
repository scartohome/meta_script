#! /bin/bash
#export AWS_ACCESS_KEY_ID=“AAKIAI7VMPIIM4GZVUIFQ"
#export AWS_SECRET_ACCESS_KEY="Cr9ynI8u3Tuum7ay+3f8NbYJZaAxJyxpRaJS/IyO"

echo '********************************************'
echo '********************************************'
echo '***          MAIN SCRIPT START           ***'
echo '********************************************'
echo '********************************************'

CWD="$(pwd)"
CDN="${CDN:-dev.ikodesh.org-E1E35JL6E3L57G}"
echo "Working remote CDN is : $CDN"
echo "Working remote DIR is : $CWD"
echo '--------------------------------------------'
Path_var=$(echo $CDN | cut -f1 -d-)
echo "Working remote PATH is : $Path_var"
echo '--------------------------------------------'
Distribution_var=$(echo $CDN | cut -f2 -d-)
echo "Working remote PATH is : $Distribution_var"
echo '--------------------------------------------'

echo '--------------------------------------------'
echo "Invalidating cloudfrond (start)"
DATE=`date '+%Y-%m-%d %H:%M:%S'`
echo "$DATE START cloudfront create-invalidation" >> "log/log.alg"
aws cloudfront create-invalidation --distribution-id=$Distribution_var --paths / --profile=default
echo '--------------------------------------------'

echo 'REMOVING old *.meta files -> rm -rf meta/*'
rm -rf meta/*
echo 'REMOVING old *.json remote files -> rm -rf json_remote/*'
rm -rf json_remote/*
echo 'REMOVING old *.json s3 files -> rm -rf json_s3/*'
rm -rf json_s3/*
echo 'REMOVING old *.json files -> rm -rf json/*'
rm -rf json/*
echo 'REMOVING old *.json DIFF -> rm -rf json_diff/*'
rm -rf json_diff/*
#echo 'REMOVING old *TIMESTAMP.json -> rm -rf timestamp/*'
#rm -rf timestamp/*

echo '--------------------------------------------'
echo 'Creating folders'
mkdir -p $CWD/meta;
mkdir -p $CWD/json;
mkdir -p $CWD/json_remote;
mkdir -p $CWD/json_s3;
mkdir -p $CWD/json_diff;
mkdir -p $CWD/timestamp;

echo '--------------------------------------------'
echo 'Starting to copy *.meta from s3... --quiet'
aws s3 cp s3://$Path_var/drive/courses/ meta/ --recursive --exclude "*" --include "*.meta" --quiet
echo 'End copy *.meta from s3...'
echo '--------------------------------------------'
echo 'Copy remote *.json files'
echo '--------------------------------------------'
aws s3 cp s3://$Path_var/drive/courses/ json_remote/ --recursive --exclude "*" --include "*.json" --quiet

echo '--------------------------------------------'
echo 'SYNC FROM S3 *.meta files - DONE'
echo 'Running Node script'
echo '--------------------------------------------'
node script.js
echo '--------------------------------------------'
echo 'Node script ended...'
echo '--------------------------------------------'
echo 'Change log - TRUE if UNCHANGED from last update'
cat $CWD/json_diff/diff_comb.json
line2="`sed -n 2p json_diff/diff_comb.json`"
echo "COMBINED DIFF is ::: $line2"
echo '--------------------------------------------'


while IFS='' read -r line || [[ -n "$line" ]]; do
    echo "Text read from file: $line"
done < "$1"


if [[ $line2 = *"false"* ]]; then
  echo -e "NO CHANGES FOUND IN *.meta -> NO CHANGES in generated JSON files
  \n -possible to DISABLE aws cloudfront create-invalidation)
  \n -possible NOT to upload unchanged jsons to S3 \n"
else
  echo -e "CHANGES FOUND IN *.meta -> UPLOADING TIMESTAMP & JSON files"
  aws s3 cp timestamp/course_2_timestamp.json s3://$Path_var/drive/courses/course_2/
  aws s3 cp timestamp/course_3_timestamp.json s3://$Path_var/drive/courses/course_3/
  aws s3 cp timestamp/course_4_timestamp.json s3://$Path_var/drive/courses/course_4/
  aws s3 cp timestamp/course_fe_timestamp.json s3://$Path_var/drive/courses/course_fe/
  echo '--------------------------------------------'
  echo 'Uploading to S3...'
  echo '--------------------------------------------'
  aws s3 cp json/course_2.json s3://$Path_var/drive/courses/course_2/
  aws s3 cp json/course_3.json s3://$Path_var/drive/courses/course_3/
  aws s3 cp json/course_4.json s3://$Path_var/drive/courses/course_4/
  aws s3 cp json/course_fe.json s3://$Path_var/drive/courses/course_fe/
fi

echo '--------------------------------------------'
echo "Invalidating cloudfrond (end)"
DATE=`date '+%Y-%m-%d %H:%M:%S'`
echo "$DATE END cloudfront create-invalidation" >> "log/log.alg"
aws cloudfront create-invalidation --distribution-id=$Distribution_var --paths / --profile=default
echo '--------------------------------------------'
echo "Deployment complete"

echo '--------------------------------------------'
echo 'S3 Bucket : check json file date/timestamp :'
echo '--------------------------------------------'

aws s3 ls s3://$Path_var/drive/courses/course_2/course_2.json
if [ $? -ne 0 ]; then
  echo "File 2 does not exist"
fi

aws s3 ls s3://$Path_var/drive/courses/course_3/course_3.json
if [ $? -ne 0 ]; then
  echo "File 3 does not exist"
fi

aws s3 ls s3://$Path_var/drive/courses/course_4/course_4.json
if [ $? -ne 0 ]; then
  echo "File 4 does not exist"
fi

aws s3 ls s3://$Path_var/drive/courses/course_fe/course_fe.json
if [ $? -ne 0 ]; then
  echo "File fe does not exist"
fi

echo '--------------------------------------------'
echo 'Local copy from S3 after update:'
echo '--------------------------------------------'

aws s3 cp s3://$Path_var/drive/courses/course_2/course_2.json json_s3/
aws s3 cp s3://$Path_var/drive/courses/course_3/course_3.json json_s3/
aws s3 cp s3://$Path_var/drive/courses/course_4/course_4.json json_s3/
aws s3 cp s3://$Path_var/drive/courses/course_fe/course_fe.json json_s3/






























#echo '--------------------------------------------'
#echo "Invalidating cloudfrond distribution to get fresh cache"
#aws cloudfront create-invalidation --distribution-id=$Distribution_var --paths / --profile=default
#echo '--------------------------------------------'
#echo "Deployment complete"

#course_2
#s3://$Path_var/drive/courses/
#aws s3 cp json/course_2.json s3://$Path_var/drive/courses/course_2/
#aws s3 cp json/course_3.json s3://$Path_var/drive/courses/course_3/
#aws s3 cp json/course_4.json s3://$Path_var/drive/courses/course_4/
#aws s3 cp json/course_fe.json s3://$Path_var/drive/courses/course_fe/

#aws s3 sync s3://$Path_var/drive/courses/ meta/ --exclude "*" --include "*.meta"

#aws configure

#dev.compedia.net/drive/courses/ ???? #anothe bucket - empty