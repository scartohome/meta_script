#! /bin/sh
echo "============================================"
CWD="$(pwd)"
echo working dir is : $CWD
echo "============================================"
echo "Running main bash_script in 10 sec"
echo "Main bash script will run now every 5 min (300 sec)"
echo "Consider using crontab, screen, service etc.."
echo "============================================"
/bin/bash -c "sleep 15 && while true; do $CWD/bash_script.sh ; sleep 300; done"
echo "============================================"
