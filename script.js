/**
 * Created by alexb on 28/03/18.
 */
var path = require('path');
var fs = require('fs');
var glob = require("glob");
var jsonfile = require('jsonfile');
var exec = require('child_process').exec;
var equal = require('deep-equal');
var os = require("os");

var jsonRemote = fs.readdirSync('./json_remote/', function (err, files) {
  if (err) {
    console.log(err);
    return;
  }
  files.map(function (file) {
    return path.join(p, file);
  }).filter(function (file) {
    return fs.statSync(file).isFile();
  })
});

var jsonRemoteArr = {};

glob("**/*.meta", {}, function (er, files) {

  // return;

  // fs.lstatSync('').isDirectory();
  var p = "./meta";
  var directoryArr = fs.readdirSync(p, function (err, files) {
    if (err) {
      console.log(err);
      return;
    }
    files.map(function (file) {
      return path.join(p, file);
    }).filter(function (file) {
      return fs.statSync(file).isFile();
    }).forEach(function (file) {
      // console.log("%s (%s)", file, path.extname(file));
    });
  });

  var list = '';

  directoryArr.forEach(function (dirName) {
    try {
      jsonRemoteArr[dirName] = require('./json_remote/' + dirName + '/' + dirName +'.json');
    } catch (ex) {
      jsonRemoteArr[dirName] = {};
    }

    fs.appendFile('list/list.alg', dirName + os.EOL, function (err) {
      if (err) throw err;
      console.log(dirName, ' to LIST saved');
    });
  });

  var count = files.length;
  console.log('--------------------------------------------');
  console.log('Remote courses : ', jsonRemote);
  console.log('Amount of courses with *META files :', directoryArr.length);
  console.log('Found ', count, ' *.meta files');

  var jsonAllCourses = {};

  directoryArr.forEach(function (dirName) {
    jsonAllCourses[dirName] = {}
  });

  var a = 0;
  files.forEach(function(file) {
    var fileData = fs.readFileSync(file, "utf8");
    metaFactory(null, fileData);
    function metaFactory(err, fileData) {
      if (err) {
        console.log(err);
        throw err;
      }

      var arrayOfLines = fileData.match(/[^\r\n]+/g);

      var pathA = file.replace('meta/', 'drive/courses/');
      pathA = pathA.replace('.meta', '');

      var string = pathA.split('/');
      var stringA = string[2];
      var stringB = string[3];

      // console.log('-------------')
      // console.log('file', file)
      // console.log('string', string)
      // // console.log('pathA', pathA)
      // // console.log('stringA', stringA)
      // // console.log('stringB', stringB)
      // console.log('-------------')

      var arrTmp = [];
      directoryArr.forEach(function (dirName) {
        if (pathA.indexOf('drive/courses/'+ dirName +'/') !== -1){
          jsonAllCourses[dirName] = jsonAllCourses[dirName] ? jsonAllCourses[dirName] : [];
          if (jsonAllCourses[dirName][stringB] &&  typeof jsonAllCourses[dirName][stringB] !== 'undefined' && jsonAllCourses[dirName][stringB][0].Hash) {
            arrTmp = jsonAllCourses[dirName][stringB];
            arrTmp.push({
                "Hash": arrayOfLines[1].replace('Hash=', ''),
                "Version": arrayOfLines[3] ? arrayOfLines[3].replace('Version=', '') : '',
                "Type": arrayOfLines[5] ? arrayOfLines[5].replace('Type=', '') : '',
                "Path": pathA
              }
            );
            jsonAllCourses[dirName][stringB] = arrSortBy(arrTmp);
          } else {
            jsonAllCourses[dirName][stringB] = [{
              "Hash": arrayOfLines[1].replace('Hash=', ''),
              "Version": arrayOfLines[3] ? arrayOfLines[3].replace('Version=', '') : '',
              "Type": arrayOfLines[5] ? arrayOfLines[5].replace('Type=', '') : '',
              "Path": pathA
            }];
          }
        }
      });
    }
  });
  console.log('--------------------------------------------');
  var done = 0;
  var filesArrLength = directoryArr.length;
  var updated = {};
  var updatedFlag = false;

  directoryArr.forEach(function (dirName) {
    jsonfile.writeFile("json/"+ dirName +".json", jsonAllCourses[dirName], {spaces: 2}, function (err, obj) {
      if(err)console.error(err);
      console.log('Created ' + dirName + ' json file');
      callbackWriteFile(++done)
    });
  });

  function callbackWriteFile(done) {
    if (done === filesArrLength) {
      console.log('--------------------------------------------');
      console.log('ALL', done, 'json files created');

      directoryArr.forEach(function (dirName){
        updated[dirName] = !equal(jsonRemoteArr[dirName], jsonAllCourses[dirName]);
        updatedFlag = updatedFlag || updated[dirName]
      });

      var logLine = {};
      updated['MAIN_FINAL'] = updatedFlag ? 'UPDATED' : "NO CHANGES";
      logLine[Date.now()] = updated;

      fs.appendFile('log/log.alg', JSON.stringify(logLine) + '\r\n', function (err) {
        if (err) throw err;
        console.log('LOG Saved!');
      });

      jsonfile.writeFile("json_diff/diff.json", updated, {spaces: 2}, function (err, obj) {
        if(err)console.error(err);
        console.log('Created DIFF json file');
      });

      jsonfile.writeFile("json_diff/diff_comb.json", [updatedFlag], {spaces: 2}, function (err, obj) {
        if(err)console.error(err);
        console.log('Created DIFF COMBINED json file');
      });

      directoryArr.forEach(function (dirName) {
        if (updated[dirName]){
          jsonfile.writeFile("timestamp/"+ dirName +"_timestamp.json", {
            "course" : dirName,
            "timestamp" : ''+(new Date()).getTime()
          }, {spaces: 2}, function (err, obj) {
            if(err)console.error(err);
            console.log('Created TIMESTAMP ' + dirName + 'json file');
          });
        }
      });

      directoryArr.forEach(function (dirName) {
        if (updated[dirName]) {
          var date = (new Date()).getTime();
          jsonfile.writeFile("updated/" + dirName + "_diff.json", {
            updated : updated[dirName],
            timestamp : date,
            local_date :  new Date(date)
          }, {spaces: 2}, function (err, obj) {
            if(err)console.error(err);
            console.log('Created UPDATED ' + dirName + ' json file');
          });
        }
      })
    }
  }
});

function arrSortBy(arr){
  return arr.sort(function (a, b) {
    if(a.Hash < b.Hash) return -1;
    if(a.Hash > b.Hash) return 1;
    return 0;
  });
}

console.log('SCRIPT LOADED, WAIT FOR ASYNC FUNCTIONS...');
